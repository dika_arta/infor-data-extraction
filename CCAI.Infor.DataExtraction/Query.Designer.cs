﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18444
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CCAI.Infor.DataExtraction {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Query {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Query() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("CCAI.Infor.DataExtraction.Query", typeof(Query).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to SELECT x.meter_date AS &quot;DATE&quot;, 
        ///       x.loc, 
        ///       x.obj_code AS asset, 
        ///       x.basis_num, 
        ///       x.plate_num, 
        ///       x.status, 
        ///       CASE x.status 
        ///              WHEN &apos;A&apos; THEN &apos;NO REASON&apos; 
        ///              ELSE x.reason 
        ///       END AS reason, 
        ///       x.meter_read, 
        ///       x.meter_diff, 
        ///       x.meter_fuelusage AS fuel, 
        ///       x.model, 
        ///       x.purpose, 
        ///       x.ownership, 
        ///       x.vehtype, 
        ///       x.company, 
        ///       x.dept, 
        ///       x.costcenter 
        ///FROM   ( 
        ///                    [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string SelectVehicleFuelAndKMByDate {
            get {
                return ResourceManager.GetString("SelectVehicleFuelAndKMByDate", resourceCulture);
            }
        }
    }
}
