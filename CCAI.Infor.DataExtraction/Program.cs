﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oracle.ManagedDataAccess.Client;
using Oracle.ManagedDataAccess.Types;
using System.Data;
using System.IO;
using System.Data.SqlClient;
using System.Configuration;

namespace CCAI.Infor.DataExtraction
{
    class Program
    {
        static void Main(string[] args)
        {
            Logger logger = LogManager.GetLogger("");

            try
            {
                OptionHelper optHelp = new OptionHelper(args);
                var opt = optHelp.SetOption();

                //set DataDate To yesterday
                opt.DataDate = opt.DataDate.AddDays(-1);

                logger.Info(string.Format("Start Process with date ({0:yyyyMMdd})", opt.DataDate));

                logger.Info("Retrieving data from Infor Database...");
                var dt = R5ObjectRepository.GetVechFuelAndKm(opt.DataDate);

                logger.Info("Converting data to CSV...");
                bool includeHeader = bool.Parse(ConfigurationManager.AppSettings["Csv.IncludeHeader"].ToString());
                string separator = ConfigurationManager.AppSettings["Csv.Separator"].ToString();
                var rows = dt.ToCSV(includeHeader,separator);

                logger.Info("Uploading CSV file to Ftp Server...");
                using (Stream s = GeneralHelper.GenerateStreamFromString(rows))
                {
                    FTPHelper.UploadCsvToFtp(s);
                }

                logger.Info("Process succeed...");  
            }
            catch(Exception ex)
            {
                logger.Error(ex.ToString());
                throw ex;
            }
        }

        private static string DummyGetDataFromSQLServer()
        {
            string conStr = @"Initial Catalog=eSICPSD_Dev;Data Source=167.103.49.160\Dev;User Id=svc_sqlfleetmate_id;Password=P@ssw0rd@Fl3eTm4tE";
            string rows = string.Empty;
            using (SqlConnection con = new SqlConnection(conStr))
            {
                con.Open();
                string query = "SELECT * FROM M_Line";
                using (var cmd = new SqlCommand(query, con))
                {
                    using (SqlDataReader dataReader = cmd.ExecuteReader())
                    {
                        rows = dataReader.ToCSV(false, ",");
                        dataReader.Close();
                    }
                }
            }
            return rows;
        }
    }
}
