﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oracle.ManagedDataAccess.Client;
using Oracle.ManagedDataAccess.Types;

namespace CCAI.Infor.DataExtraction
{
    public class R5ObjectRepository : BaseRepository
    {
        public static DataTable GetVechFuelAndKm(DateTime date)
        {
            using (var con = OpenConnection())
            {
                DataTable dt = new DataTable();
                string sql = String.Format(Query.SelectVehicleFuelAndKMByDate,date.ToString("yyyy-MM-dd"));
                //string sql = "SELECT * FROM DUMMY_INFOR";
                OracleCommand cmd = new OracleCommand(sql, con);
                //cmd.Parameters.Add(new OracleParameter("DATE", date.ToString("yyyy-MM-dd")));
                cmd.CommandType = CommandType.Text;

                var dr = cmd.ExecuteReader();
                dt.Load(dr);
                return dt;
            }
        }
    }
}
