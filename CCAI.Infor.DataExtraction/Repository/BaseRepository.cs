﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oracle.ManagedDataAccess.Client;
using Oracle.ManagedDataAccess.Types;

namespace CCAI.Infor.DataExtraction
{
    public class BaseRepository
    {
        protected static OracleConnection OpenConnection()
        {
            string userId = ConfigurationManager.AppSettings["Oracle.UserId"].ToString();
            string userPass = ConfigurationManager.AppSettings["Oracle.UserPass"].ToString();

            string oradb = string.Format("Data Source=InforDataSource;User Id={0};Password={1};",userId,userPass);
            var dbConnection = new OracleConnection(oradb);
            dbConnection.Open();
            return dbConnection;
        }
    }
}
