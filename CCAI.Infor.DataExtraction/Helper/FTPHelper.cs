﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace CCAI.Infor.DataExtraction
{
    public class FTPHelper
    {
        /// <summary>
        /// Upload File With Ftp
        /// https://msdn.microsoft.com/en-us/library/ms229715%28v=vs.110%29.aspx?f=255&MSPPError=-2147217396
        /// </summary>
        /// <param name="csvStream"></param>
        public static void UploadCsvToFtp(Stream csvStream)
        {
            var hostname = ConfigurationManager.AppSettings["FTP.Host"].ToString();
            var user = ConfigurationManager.AppSettings["FTP.User"].ToString();
            var password = ConfigurationManager.AppSettings["FTP.Password"].ToString();
            var fileName = ConfigurationManager.AppSettings["FTP.FileName"].ToString();

            // Get the object used to communicate with the server.
            string requestUriString = String.Format("ftp://{0}/{1}", hostname,fileName);
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(requestUriString);
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Proxy = new WebProxy();

            // This example assumes the FTP site uses anonymous logon.
            request.Credentials = new NetworkCredential(user, password);

            // Copy the contents of the file to the request stream.
            StreamReader sourceStream = new StreamReader(csvStream);
            byte[] fileContents = Encoding.UTF8.GetBytes(sourceStream.ReadToEnd());
            sourceStream.Close();
            request.ContentLength = fileContents.Length;

            Stream requestStream = request.GetRequestStream();
            requestStream.Write(fileContents, 0, fileContents.Length);
            requestStream.Close();

            FtpWebResponse response = (FtpWebResponse)request.GetResponse();
            response.Close();
        }
    }
}
