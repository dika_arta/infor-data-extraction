﻿using NDesk.Options;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CCAI.Infor.DataExtraction
{
    public class OptionHelper
    {
        private Options _opt;
        private string[] _args { get; set; }

        public OptionHelper(string[] args)
        {
            _args = args;
            _opt = new Options();
            _opt.DataDate = DateTime.Now;
        }

        public Options SetOption()
        {
            var p = new OptionSet
                        {
                           
                            {"d|date=", 
                                "the date of data to be generated, date format is yyyy-MM-dd", 
                                v => ParseDataDateOption(v, ref _opt)},
                            
                        };

            try
            {
                p.Parse(_args);
            }
            catch (OptionException ex)
            {
                Console.WriteLine(string.Format("Error for option '{0}': {1}", ex.OptionName, ex.Message));
                throw ex;
            }

            return _opt;
        }

        private void ParseDataDateOption(string v, ref Options option)
        {
            try
            {
                option.DataDate = DateTime.ParseExact(v, "yyyy-MM-dd", new CultureInfo("en-US"));
            }
            catch (FormatException ex)
            {
                throw new OptionException(string.Format("Unable to parse {0}", v), "date", ex);
            }
        }
    }

    public class Options
    {
        public DateTime DataDate;
    }
}
